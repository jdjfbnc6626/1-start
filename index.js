// Noted documentation bug - imports - in Zoom meet
const {
    getAllUsers,
    getUserByID,
    createNewUser,
    updateUserByID
} = require('./user/user');

// console.log(getAllUsers());

console.log(getUserByID(1) || 'No user found');

console.log(
    createNewUser({
        fName: 'Test',
        lName: 'User',
        username: 'testUser',
    })
);

console.log(updateUserByID(2, { fName: 'example', username: 'example' }));
